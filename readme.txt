Implemented
***********
- Used Angular JS
- Used bootstrap for css style sheet
- The site shows the user's current location (City, Country)
- When user type the city name and submit, it will shows the air quality index.
- Current Location API : https://api.ipgeolocation.io/ipgeo?apiKey=a4f470857af540e5baecf0c510d611cb
- Air Quality Index API : https://api.waqi.info/feed/yangon/?token=e7cbab0d01f9b6766e481b4c8fc9da6516fe64ba

Can be improved
****************
- Instead of clicking on submit button, user should be able to submit by pressing Enter Key after typing a city name
