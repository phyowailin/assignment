/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('airasiaApp').controller('Controller', function ($scope, Service) {

    $scope.city;
    $scope.status;
    $scope.message;
    $scope.location;
    loadCurrentLocation();

    function loadAirQtyIndex() {

        Service.loadAirQuality($scope.city)
                .then(
                        function (data) {

                            if (data["status"] == 'ok') {
                                $scope.status = "Status : " + data["status"];
                                res = data["data"];
                                $scope.message = "Air Quality Index of " + $scope.city + " : " + res["aqi"];
                            } else
                            {
                                $scope.status = "Status : " + data["status"];
                                $scope.message = "Reason : " + data["data"];
                            }

                        },
                        function (errResponse) {
                            console.error('Error while fetching AirQuality');
                        }
                );
    }

    $scope.showIndex = function ()
    {
        loadAirQtyIndex();
    }

    function loadCurrentLocation() {

        Service.loadCurrentLocation()
                .then(
                        function (data) {
                            $scope.location = "Current Location : " + data["city"] + ", " + data["country_name"];
                        },
                        function (errResponse) {
                            console.error('Error while fetching Current Location');
                        }
                );
    }
});

