/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';

angular.module('airasiaApp').factory('Service', ['$http', '$q', function ($http, $q) {        

        var factory = {
            loadAirQuality: loadAirQuality,
            loadCurrentLocation: loadCurrentLocation            
        };

        return factory;        

        function loadAirQuality(city) {
            var deferred = $q.defer();
            $http.get("https://api.waqi.info/feed/"+city+"/?token=e7cbab0d01f9b6766e481b4c8fc9da6516fe64ba")
                    .then(
                            function (response) {
                                deferred.resolve(response.data);
                            },
                            function (errResponse) {
                                console.error('Error while fetching Air Quality Index');
                                deferred.reject(errResponse);
                            }
                    );
            return deferred.promise;
        }
        
        function loadCurrentLocation() {
            var deferred = $q.defer();
            $http.get("https://api.ipgeolocation.io/ipgeo?apiKey=a4f470857af540e5baecf0c510d611cb")
                    .then(
                            function (response) {
                                deferred.resolve(response.data);
                            },
                            function (errResponse) {
                                console.error('Error while fetching Current Location');
                                deferred.reject(errResponse);
                            }
                    );
            return deferred.promise;
        }

        

    }]);

